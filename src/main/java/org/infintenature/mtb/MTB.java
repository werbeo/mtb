package org.infintenature.mtb;

/**
 * Imutable representation of a mtb
 *
 * @author dve
 *
 */
public class MTB
{
   public enum Size
   {
      MTB, MTBQ, MTBQQ, MTBQQQ
   }

   private static final String EPSG = "EPSG:4314";
   private static final int EPSG_NUMBER = 4314;

   public static String getEpsg()
   {
      return EPSG;
   }

   public static int getEpsgNumber()
   {
      return EPSG_NUMBER;
   }

   private final Rational eastEdge;
   private final String mtb;
   private final Rational northEdge;
   private final Rational southEdge;
   private final Rational westEdge;
   private final String name;
   private Size size;

   MTB(String mtb, Rational northEdge, Rational southEdge, Rational westEdge,
	 Rational eastEdge, String name)
   {
      super();
      this.mtb = mtb;
      this.northEdge = northEdge;
      this.southEdge = southEdge;
      this.westEdge = westEdge;
      this.eastEdge = eastEdge;
      this.name = name;
   }

   @Override
   public int hashCode()
   {
      final int prime = 31;
      int result = 1;
      result = prime * result + ((eastEdge == null) ? 0 : eastEdge.hashCode());
      result = prime * result + ((mtb == null) ? 0 : mtb.hashCode());
      result = prime * result
	    + ((northEdge == null) ? 0 : northEdge.hashCode());
      result = prime * result
	    + ((southEdge == null) ? 0 : southEdge.hashCode());
      result = prime * result + ((westEdge == null) ? 0 : westEdge.hashCode());
      return result;
   }

   @Override
   public boolean equals(Object obj)
   {
      if (this == obj)
      {
         return true;
      }
      if (obj == null)
      {
         return false;
      }
      if (getClass() != obj.getClass())
      {
         return false;
      }
      MTB other = (MTB) obj;
      if (eastEdge == null)
      {
	 if (other.eastEdge != null)
   {
      return false;
   }
      } else if (!eastEdge.equals(other.eastEdge))
      {
         return false;
      }
      if (mtb == null)
      {
	 if (other.mtb != null)
   {
      return false;
   }
      } else if (!mtb.equals(other.mtb))
      {
         return false;
      }
      if (northEdge == null)
      {
	 if (other.northEdge != null)
   {
      return false;
   }
      } else if (!northEdge.equals(other.northEdge))
      {
         return false;
      }
      if (southEdge == null)
      {
	 if (other.southEdge != null)
   {
      return false;
   }
      } else if (!southEdge.equals(other.southEdge))
      {
         return false;
      }
      if (westEdge == null)
      {
	 if (other.westEdge != null)
   {
      return false;
   }
      } else if (!westEdge.equals(other.westEdge))
      {
         return false;
      }
      return true;
   }

   public Rational getEastEdge()
   {
      return eastEdge;
   }

   public String getMtb()
   {
      return mtb;
   }

   public Rational getNorthEdge()
   {
      return northEdge;
   }

   public Rational getSouthEdge()
   {
      return southEdge;
   }

   public Rational getWestEdge()
   {
      return westEdge;
   }

   @Override
   public String toString()
   {
      StringBuilder builder = new StringBuilder();
      builder.append("MTB [mtb=");
      builder.append(mtb);
      builder.append(", northEdge=");
      builder.append(northEdge);
      builder.append(", southEdge=");
      builder.append(southEdge);
      builder.append(", westEdge=");
      builder.append(westEdge);
      builder.append(", eastEdge=");
      builder.append(eastEdge);
      builder.append("]");
      return builder.toString();
   }

   public String toWkt()
   {
      StringBuilder sb = new StringBuilder("POLYGON((");
      sb.append(westEdge.toDouble());
      sb.append(" ");
      sb.append(southEdge.toDouble());
      sb.append(",");
      sb.append(eastEdge.toDouble());
      sb.append(" ");
      sb.append(southEdge.toDouble());
      sb.append(",");
      sb.append(eastEdge.toDouble());
      sb.append(" ");
      sb.append(northEdge.toDouble());
      sb.append(",");
      sb.append(westEdge.toDouble());
      sb.append(" ");
      sb.append(northEdge.toDouble());
      sb.append(",");
      sb.append(westEdge.toDouble());
      sb.append(" ");
      sb.append(southEdge.toDouble());
      sb.append("))");
      return sb.toString();
   }

   /**
    *
    * @return the center of the mtb as long, lat
    */
   public Rational[] getCenter()
   {
      Rational longitude = westEdge.plus(eastEdge).divides(new Rational(2));
      Rational latitude = northEdge.plus(southEdge).divides(new Rational(2));
      return new Rational[] { longitude, latitude };
   }

   public String getCenterWkt()
   {
      Rational[] center = getCenter();
      double longitude = center[0].toDouble();
      double latitude = center[1].toDouble();
      return createWktPoint(longitude, latitude);
   }

   private String createWktPoint(double longitude, double latitude)
   {
      StringBuilder sb = new StringBuilder("POINT(");
      sb.append(longitude);
      sb.append(' ');
      sb.append(latitude);
      sb.append(")");
      return sb.toString();
   }

   public String[] getCornerWkts()
   {
      String[] corners = {
	    createWktPoint(westEdge.toDouble(), southEdge.toDouble()),
	    createWktPoint(westEdge.toDouble(), northEdge.toDouble()),
	    createWktPoint(eastEdge.toDouble(), southEdge.toDouble()),
	    createWktPoint(eastEdge.toDouble(), northEdge.toDouble()) };
      return corners;
   }

   public Size getSize()
   {
      if (size == null)
      {
	 switch (mtb.length())
	 {
	 case 4:
	    size = Size.MTB;
	    break;
	 case 6:
	    size = Size.MTBQ;
	    break;
	 case 7:
	    size = Size.MTBQQ;
	    break;
	 case 8:
	    size = Size.MTBQQQ;
	    break;
	 default:
	    break;
	 }
      }
      return size;
   }

   public String getName()
   {
      return name;
   }
}
