package org.infintenature.mtb;

import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.geotools.geometry.GeometryBuilder;
import org.geotools.geometry.iso.text.WKTParser;
import org.geotools.referencing.CRS;
import org.infintenature.mtb.MTB.Size;
import org.opengis.geometry.DirectPosition;
import org.opengis.geometry.Geometry;
import org.opengis.referencing.crs.CRSAuthorityFactory;
import org.opengis.referencing.crs.CoordinateReferenceSystem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class to convert german mtb notation to different notations.
 * <p>
 * Based on the mtb_helper of the indicia project.
 * <p>
 *
 * @see <a href=
 *      "http://code.google.com/p/indicia/source/browse/core/trunk/modules/sref_mtb/helpers/mtbqqq.php">PHP
 *      source</a>
 *
 * @author dve
 *
 */
public class MTBHelper
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(MTBHelper.class);
   private static final Rational gridOriginX = new Rational(1);
   private static final Rational gridOriginY = new Rational(9);
   private static final Rational sixMinutes = new Rational(1, 10);
   private static final Rational tenMinutes = new Rational(1, 6);
   private static final Rational originX = new Rational(35, 6);
   private static final Rational originY = new Rational(551, 10);

   private static final Pattern MTB_PATTERN = Pattern
         .compile("^\\d\\d\\d\\d(\\/[1-4][1-4]?[1-4]?)?$");
   private static final Properties mtbNames = new Properties();
   private static final Set<MTB> mtbs;

   public static final MTB nullMTB = new MTB("0000/000", new Rational(0),
         new Rational(0), new Rational(0), new Rational(0), "NullMTB");

   static
   {
      try
      {
         mtbNames.load(MTBHelper.class.getClassLoader()
               .getResourceAsStream("mtb.properties"));

      } catch (IOException e)
      {
         LOGGER.error("Can't initialize MTBs.", e);
         throw new RuntimeException("Can't initialize MTBs.", e);
      }
      Set<MTB> mutableMTBs = new HashSet<>();
      for (Object o : mtbNames.keySet())
      {
         mutableMTBs.add(toMTB(o.toString()));
      }
      mtbs = Collections.unmodifiableSet(mutableMTBs);
   }

   private MTBHelper()
   {
      throw new IllegalAccessError("Utility class");
   }

   public static Set<MTB> getAllMTBs()
   {
      return new HashSet<>(mtbs);
   }

   public static MTB toMTB(String mtbString)
   {
      LOGGER.trace("Getting wkt of mtb: {} ", mtbString);

      if (StringUtils.equals(mtbString, nullMTB.getMtb()))
      {
         return nullMTB;
      }

      if (!isValid(mtbString))
      {
         throw new IllegalArgumentException(
               "Spatial reference is not a recognisable grid square: "
                     + mtbString);
      }
      Rational yy = new Rational(Integer.valueOf(mtbString.substring(0, 2)));
      Rational xx = new Rational(Integer.valueOf(mtbString.substring(2, 4)));
      LOGGER.trace("y of mtb = {}", yy);
      LOGGER.trace("x of mtb = {}", xx);
      // Top left cell of grid system is 0901
      yy = yy.minus(gridOriginY);
      xx = xx.minus(gridOriginX);
      LOGGER.trace("y of mtb = {}", yy);
      LOGGER.trace("x of mtb = {}", xx);
      Rational northEdge = originY.minus(yy.times(sixMinutes));
      Rational westEdge = originX.plus(xx.times(tenMinutes));
      // we now have the top left of the outer grid square. We need to work
      // out the quadrants.
      // Loop through the quadrant digits.
      for (int i = 5; i < mtbString.length(); i++)
      {
         int q = Integer.parseInt(mtbString.substring(i, i + 1));
         if (q > 2)
         {
            northEdge = northEdge.minus(
                  sixMinutes.divides(new Rational((int) Math.pow(2, i - 4))));
         }
         if (q == 2 || q == 4)
         {
            westEdge = westEdge.plus(
                  tenMinutes.divides(new Rational((int) Math.pow(2, i - 4))));
         }
      }
      // we now have the top left of a grid square. Need to know all the
      // edges. Work out the amount we must
      // divide a full size grid square edge by to find the quadrant size.
      int sizing = 1;
      if (mtbString.length() >= 6)
      {
         sizing = (int) Math.pow(2, mtbString.length() - 5); // 2, 4 or 8
      }
      LOGGER.trace("sizing = {}", sizing);
      Rational southEdge = northEdge
            .minus(sixMinutes.divides(new Rational(sizing)));
      Rational eastEdge = westEdge
            .plus(tenMinutes.divides(new Rational(sizing)));
      return new MTB(mtbString, northEdge, southEdge, westEdge, eastEdge,
            mtbNames.getProperty(mtbString.substring(0, 4)));
   }

   public static boolean isValid(String mtb)
   {
      LOGGER.trace("testing if {} is a valid mtb", mtb);
      Matcher matcher = MTB_PATTERN.matcher(mtb);
      return matcher.matches();
   }

   public static Geometry toGeometry(String mtb)
   {
      String wkt = toMTB(mtb).toWkt();
      LOGGER.trace("wkt: {}", wkt);
      try
      {
         CRSAuthorityFactory factory = CRS.getAuthorityFactory(true);
         CoordinateReferenceSystem _4745 = factory
               .createCoordinateReferenceSystem(getSrid());
         GeometryBuilder gb = new GeometryBuilder(getSrid());
         WKTParser parser = new WKTParser(gb);
         return parser.parse(wkt);
      } catch (Exception e)
      {
         LOGGER.error("Failure getting geometry from mtb", e);
         return null;
      }
   }

   public static String getSrid()
   {
      return "EPSG:4745";
   }

   public static double getFuzzines(MTB mtb)
   {
      try
      {
         CRSAuthorityFactory factory = CRS.getAuthorityFactory(true);
         CoordinateReferenceSystem _4745 = factory
               .createCoordinateReferenceSystem(getSrid());
         CoordinateReferenceSystem _31468 = factory
               .createCoordinateReferenceSystem("EPSG:31468");

         GeometryBuilder gb = new GeometryBuilder(_4745);
         WKTParser parser = new WKTParser(gb);
         String[] cornersWkt = mtb.getCornerWkts();

         Geometry center = parser.parse(mtb.getCenterWkt()).transform(_31468);
         double fuzzines = 0;
         for (String cornerWkt : cornersWkt)
         {
            Geometry corner = parser.parse(cornerWkt).transform(_31468);
            fuzzines = Math.max(fuzzines, corner.distance(center));
         }
         return fuzzines;
      } catch (Exception e)
      {
         LOGGER.error("something bad happend", e);
         return 0;
      }
   }

   public static double getHW(MTB mtb)
   {
      int index = 1;
      return getHRW(mtb, index);
   }

   public static double getRW(MTB mtb)
   {
      int index = 0;
      return getHRW(mtb, index);
   }

   private static double getHRW(MTB mtb, int index)
   {
      try
      {
         CRSAuthorityFactory factory = CRS.getAuthorityFactory(true);
         CoordinateReferenceSystem _4745 = factory
               .createCoordinateReferenceSystem(getSrid());
         CoordinateReferenceSystem _31468 = factory
               .createCoordinateReferenceSystem("EPSG:31468");

         GeometryBuilder gb = new GeometryBuilder(_4745);
         WKTParser parser = new WKTParser(gb);
         Geometry center = parser.parse(mtb.getCenterWkt()).transform(_31468);
         return center.getCentroid().getCoordinate()[index];
      } catch (Exception e)
      {
         LOGGER.error("something bad happend", e);
         return 0;
      }
   }

   /**
    * Converts a DirectPositon to a MTB
    *
    * @param centroid
    *           in EPSG:4745
    * @return the MTB in which the centroid is located
    */
   public static String fromCentroid(DirectPosition centroid)
   {
      double easting = centroid.getCoordinate()[0];
      double northing = centroid.getCoordinate()[1];
      return fromCentroid(easting, northing);
   }

   /**
    * Converts a point to a MTB
    *
    * @param easting
    *           in EPSG:4745
    * @param northing
    *           in EPSG:4745
    * @return the MTB in which the point is located
    */
   public static String fromCentroid(double easting, double northing)
   {
      double origin_x = originX.toDouble();
      double origin_y = originY.toDouble();
      double gridorigin_x = gridOriginX.toDouble();
      double gridorigin_y = gridOriginY.toDouble();
      double sixminutes = sixMinutes.toDouble();

      double y = ((origin_y - northing) / sixminutes) + gridorigin_y;
      double x = ((easting - origin_x) * 6) + gridorigin_x;

      int yy = (int) y;
      int xx = (int) x;

      if (yy < 1 || xx < 1 || yy > 99 || xx > 99)
      {
         LOGGER.info("Coordinates out of MTB Bounds");
         return nullMTB.getMtb();
      } else
      {
         int y8th = (int) Math.floor((y - yy) * 8) + 1;
         int x8th = (int) Math.floor((x - xx) * 8) + 1;

         // start on 111
         int q1 = 1;
         int q2 = 1;
         int q3 = 1;

         // Work out each shift according to y8th
         if (y8th == 5 || y8th == 6 || y8th == 7 || y8th == 8)
         {
            q1 = 3;
         }
         if (y8th == 3 || y8th == 4 || y8th == 7 || y8th == 8)
         {
            q2 = 3;
         }
         if (y8th == 2 || y8th == 4 || y8th == 6 || y8th == 8)
         {
            q3 = 3;
         }
         // Work out each additional shift according to x8th
         if (x8th == 5 || x8th == 6 || x8th == 7 || x8th == 8)
         {
            q1++;
         }
         if (x8th == 3 || x8th == 4 || x8th == 7 || x8th == 8)
         {
            q2++;
         }
         if (x8th == 2 || x8th == 4 || x8th == 6 || x8th == 8)
         {
            q3++;
         }

         return String.format("%02d%02d/%d%d%d", yy, xx, q1, q2, q3);
      }

   }

   public static Set<MTB> getContainingMTBs(MTB mtb)
   {
      Set<MTB> mtbs = new HashSet<>();
      mtbs.add(mtb);
      for (int q = 1; q <= 4; q++)
      {
         if (mtb.getSize() == Size.MTBQQQ)
         {
            mtbs.add(mtb);
         } else
         {
            if (mtb.getSize() == Size.MTB)
            {
               mtbs.addAll(getContainingMTBs(toMTB(mtb.getMtb() + "/" + q)));
            } else
            {
               mtbs.addAll(getContainingMTBs(toMTB(mtb.getMtb() + q)));
            }
         }
      }
      return mtbs;
   }

   public static Set<MTB> getIntersectingMTBs(MTB mtb)
   {
      Set<MTB> intersecting = getContainingMTBs(mtb);
      intersecting.addAll(getParrentMTBs(mtb));
      return intersecting;
   }

   private static Set<MTB> getParrentMTBs(MTB mtb)
   {
      Set<MTB> parrentMTB = new HashSet<>();
      if (mtb.getSize() == Size.MTB)
      {

      } else
      {
         if (mtb.getSize() == Size.MTBQ)
         {
            parrentMTB.add(MTBHelper.toMTB(mtb.getMtb().substring(0, 4)));
         } else
         {
            MTB directParrent = MTBHelper
                  .toMTB(mtb.getMtb().substring(0, mtb.getMtb().length() - 1));
            parrentMTB.add(directParrent);
            parrentMTB.addAll(getParrentMTBs(directParrent));
         }
      }
      return parrentMTB;
   }
}