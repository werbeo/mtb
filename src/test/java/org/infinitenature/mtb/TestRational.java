package org.infinitenature.mtb;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.infintenature.mtb.Rational;
import org.junit.jupiter.api.Test;

public class TestRational
{

   @Test
   public void testToDouble()
   {
      Rational r = new Rational(10, 1);
      assertEquals(10.0, r.toDouble(), .001);
      assertEquals(55.1, new Rational(551, 10).toDouble(), .001);
      assertEquals(10.0, new Rational(10).toDouble(), .001);
   }

   @Test
   public void testPlus()
   {
      assertEquals(new Rational(2), new Rational(1).plus(new Rational(1)));
      assertEquals(new Rational(2),
            new Rational(1, 2).plus(new Rational(3, 2)));
      assertEquals(new Rational(5, 6),
            new Rational(1, 2).plus(new Rational(1, 3)));
      // 8/9 + 1/9 = 1
      assertEquals(new Rational(1),
            new Rational(8, 9).plus(new Rational(1, 9)));
      // 1/200000000 + 1/300000000 = 1/120000000
      assertEquals(new Rational(1, 120000000),
            new Rational(1, 200000000).plus(new Rational(1, 300000000)));
      // 1073741789/20 + 1073741789/30 = 1073741789/12
      assertEquals(new Rational(1073741789, 12),
            new Rational(1073741789, 20).plus(new Rational(1073741789, 30)));
   }

   @Test
   public void testTimes()
   {
      // 4/17 * 17/4 = 1
      assertEquals(new Rational(1),
            new Rational(4, 17).times(new Rational(17, 4)));
      // 3037141/3247033 * 3037547/3246599 = 841/961
      assertEquals(new Rational(841, 961), new Rational(3037141, 3247033)
            .times(new Rational(3037547, 3246599)));
   }

   @Test
   public void testMinus()
   {
      // 1/6 - -4/-8 = -1/3
      assertEquals(new Rational(-1, 3),
            new Rational(1, 6).minus(new Rational(-4, -8)));
   }
}
