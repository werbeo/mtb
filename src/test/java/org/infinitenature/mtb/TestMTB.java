package org.infinitenature.mtb;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.infintenature.mtb.MTB.Size;
import org.infintenature.mtb.MTBHelper;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class TestMTB
{

   @Test
   @DisplayName("Test size MTB")
   public void test001()
   {
      assertEquals(Size.MTB, MTBHelper.toMTB("1234").getSize());
   }

   @Test
   @DisplayName("Test size MTBQ")
   public void test002()
   {
      assertEquals(Size.MTBQ, MTBHelper.toMTB("1234/1").getSize());
   }

   @Test
   @DisplayName("Test size MTBQQ")
   public void test003()
   {
      assertEquals(Size.MTBQQ, MTBHelper.toMTB("1234/12").getSize());
   }

   @Test
   @DisplayName("Test size MTBQQQ")
   public void test004()
   {
      assertEquals(Size.MTBQQQ, MTBHelper.toMTB("1234/123").getSize());
   }

}
