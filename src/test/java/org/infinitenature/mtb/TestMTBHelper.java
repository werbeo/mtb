package org.infinitenature.mtb;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Collection;
import java.util.Set;

import org.geotools.geometry.GeometryBuilder;
import org.geotools.referencing.CRS;
import org.infintenature.mtb.MTB;
import org.infintenature.mtb.MTBHelper;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.opengis.geometry.DirectPosition;
import org.opengis.geometry.Geometry;
import org.opengis.referencing.crs.CRSAuthorityFactory;
import org.opengis.referencing.crs.CoordinateReferenceSystem;
import org.opengis.referencing.operation.MathTransform;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@RunWith(Parameterized.class)
public class TestMTBHelper
{
   private static final Logger logger = LoggerFactory
         .getLogger(TestMTBHelper.class);
   private String mtbToTest;
   private String mtbName;

   @Parameterized.Parameters
   public static Collection<Object[]> testFixtures()
   {
      Object[][] data = new Object[][] { { "2834/444", "Gorlosen" },
            { "2834/44", "Gorlosen" }, { "2834/4", "Gorlosen" },
            { "2834", "Gorlosen" }, { "7929", "Bad Wörishofen" },
            { "0916", "List (Sylt)" } };
      return Arrays.asList(data);
   }

   public TestMTBHelper(String mtbToTest, String mtbName)
   {
      this.mtbToTest = mtbToTest;
      this.mtbName = mtbName;
   }

   @Test
   public void test()
   {
      MTB mtb = MTBHelper.toMTB("2834/444");
      System.out.println(mtb.toWkt());
      System.out.println(mtb.getCenter()[0].toDouble() + " / "
            + mtb.getCenter()[1].toDouble());
      // assertEquals(wkt_indicia, wkt_mtbHelper);
   }

   @Test
   public void test2()
   {
      MTB mtb = MTBHelper.toMTB("2440/1");
      System.out.println(mtb.toWkt());
      System.out.println(mtb.getCenter()[0].toDouble() + " / "
            + mtb.getCenter()[1].toDouble());
      // assertEquals(wkt_indicia, wkt_mtbHelper);
   }

   @Test
   public void testFromPoint()
   {
      Geometry geometry = MTBHelper.toGeometry(mtbToTest);
      DirectPosition centroid = geometry.getCentroid();
      assertEquals(mtbToTest,
            MTBHelper.fromCentroid(centroid).substring(0, mtbToTest.length()));
   }

   @Test
   public void testMTBName()
   {
      MTB mtb = MTBHelper.toMTB(mtbToTest);
      assertThat(mtb.getName(), is(mtbName));
   }

   @Test
   public void testIsValid()
   {
      assertFalse(MTBHelper.isValid("123"));
      assertFalse(MTBHelper.isValid("WrongMTB"));
      assertFalse(MTBHelper.isValid("1234-1"));
      assertFalse(MTBHelper.isValid("1234-5"));
      assertFalse(MTBHelper.isValid("1234-1111"));
   }

   @Test
   @Ignore
   public void testGedSrid()
   {
      assertEquals("EPSG:4745", MTBHelper.getSrid());

      assertEquals(
            "POLYGON((11.479166666667 53.1,11.479166666667 53.1125,11.5 53.1125,11.5 53.1,11.479166666667 53.1))",
            MTBHelper.toMTB("2834/444").toWkt());
   }

   @Test
   @Ignore
   public void testGetGeometry() throws Exception
   {
      Geometry geometry = MTBHelper.toGeometry("2641");
      System.out.println(geometry.getCoordinateReferenceSystem());
      System.out.println(geometry);
      CRSAuthorityFactory factory = CRS.getAuthorityFactory(true);
      Geometry geometry31468 = geometry
            .transform(factory.createCoordinateReferenceSystem("EPSG:31468"));
      System.out.println(geometry31468.getCoordinateReferenceSystem());
      System.out.println(geometry31468);
      System.out.println(geometry31468.transform(CRS.decode("EPSG:900913")));
   }

   @Test
   public void testGemometry() throws Exception
   {
      CRSAuthorityFactory factory = CRS.getAuthorityFactory(true);
      CoordinateReferenceSystem _4745 = factory
            .createCoordinateReferenceSystem("EPSG:4745");
      CoordinateReferenceSystem _31468 = factory
            .createCoordinateReferenceSystem("EPSG:31468");
      GeometryBuilder gb = new GeometryBuilder(_4745);
      MathTransform transform = CRS.findMathTransform(_4745, _31468);
      double[] _src = { 12.5, 53.3 };
      DirectPosition source = gb.createDirectPosition(_src);
      DirectPosition dest = transform.transform(source, null);
      System.out.println(dest);
   }

   @Test
   public void testGetFuzzynes() throws Exception
   {
      System.out.println(MTBHelper.getFuzzines(MTBHelper.toMTB("1346")));
      System.out.println(MTBHelper.getFuzzines(MTBHelper.toMTB("1346/2")));
      System.out.println(MTBHelper.getFuzzines(MTBHelper.toMTB("1346/12")));
      System.out.println(MTBHelper.getFuzzines(MTBHelper.toMTB("1346/112")));
      System.out.println(MTBHelper.getFuzzines(MTBHelper.toMTB("2347/112")));
   }

   @Test
   public void testGetHW() throws Exception
   {
      System.out.println(MTBHelper.getHW(MTBHelper.toMTB("1346")));
      System.out.println(MTBHelper.getRW(MTBHelper.toMTB("1346")));
   }

   @Test
   public void testGetContainingMTBs()
   {
      assertEquals(85,
            MTBHelper.getContainingMTBs(MTBHelper.toMTB("1943")).size());
      assertEquals(1,
            MTBHelper.getContainingMTBs(MTBHelper.toMTB("1943/111")).size());

      Set<MTB> intersecting1943_11 = MTBHelper
            .getContainingMTBs(MTBHelper.toMTB("1943/11"));
      assertEquals(5, intersecting1943_11.size());
      assertTrue(intersecting1943_11.contains(MTBHelper.toMTB("1943/11")));
      assertTrue(intersecting1943_11.contains(MTBHelper.toMTB("1943/111")));
      assertTrue(intersecting1943_11.contains(MTBHelper.toMTB("1943/112")));
      assertTrue(intersecting1943_11.contains(MTBHelper.toMTB("1943/113")));
      assertTrue(intersecting1943_11.contains(MTBHelper.toMTB("1943/114")));
   }

   @Test
   public void testGetIntersectingMTBs()
   {
      assertEquals(85,
            MTBHelper.getIntersectingMTBs(MTBHelper.toMTB("1943")).size());
      Set<MTB> intersecting1943_111 = MTBHelper
            .getIntersectingMTBs(MTBHelper.toMTB("1943/111"));
      System.out.println(intersecting1943_111);
      assertEquals(4, intersecting1943_111.size());

      Set<MTB> intersecting1943_11 = MTBHelper
            .getIntersectingMTBs(MTBHelper.toMTB("1943/11"));
      assertEquals(7, intersecting1943_11.size());
      assertTrue(intersecting1943_11.contains(MTBHelper.toMTB("1943")));
      assertTrue(intersecting1943_11.contains(MTBHelper.toMTB("1943/1")));
      assertTrue(intersecting1943_11.contains(MTBHelper.toMTB("1943/11")));
      assertTrue(intersecting1943_11.contains(MTBHelper.toMTB("1943/111")));
      assertTrue(intersecting1943_11.contains(MTBHelper.toMTB("1943/112")));
      assertTrue(intersecting1943_11.contains(MTBHelper.toMTB("1943/113")));
      assertTrue(intersecting1943_11.contains(MTBHelper.toMTB("1943/114")));
   }
}
