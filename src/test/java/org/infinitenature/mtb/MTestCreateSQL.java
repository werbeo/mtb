package org.infinitenature.mtb;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.infintenature.mtb.MTB;
import org.infintenature.mtb.MTBHelper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class MTestCreateSQL
{

   @BeforeEach
   void setUp() throws Exception
   {
   }

   @Test
   void test()
   {
      List<String> mtbs = MTBHelper.getAllMTBs().stream()
            .map(mtb -> mtb.getMtb()).collect(Collectors.toList());
      mtbs.sort((m1, m2) -> m1.compareTo(m2));
      try (Writer writer = new BufferedWriter(new FileWriter("mtb.sql")))
      {
         for (String mtbString : mtbs)
         {
            MTB mtb = MTBHelper.toMTB(mtbString);
            addLine(writer, mtb);
            for (String q : Arrays.asList(new String[] { "1", "2", "3", "4" }))
            {
               MTB mtbq = MTBHelper.toMTB(mtbString + "/" + q);
               addLine(writer, mtbq);
               for (String qq : Arrays
                     .asList(new String[] { "1", "2", "3", "4" }))
               {
                  MTB mtbqq = MTBHelper.toMTB(mtbString + "/" + q + qq);
                  addLine(writer, mtbqq);
                  for (String qqq : Arrays
                        .asList(new String[] { "1", "2", "3", "4" }))
                  {
                     MTB mtbqqq = MTBHelper
                           .toMTB(mtbString + "/" + q + qq + qqq);
                     addLine(writer, mtbqqq);
                  }
               }
            }
         }
      } catch (IOException e)
      {
         e.printStackTrace();
      }
   }

   private void addLine(Writer writer, MTB mtb) throws IOException
   {
      writer.write(String.format(
            "INSERT INTO mtb.mtb VALUES ('%s', '%s', public.ST_Transform(public.ST_GeomFromText('%S', 4314),900913), public.ST_GeomFromText('%S', 4314));\n",
            mtb.getMtb(), mtb.getName(), mtb.toWkt(), mtb.toWkt()));
   }

}
